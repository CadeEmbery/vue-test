import HomeRoute from './Home.vue';
import GalleryRoute from './Gallery.vue';


var array = [];

///////////////////////////////////

array.push({
	path:'/',
	title:'Home',
	component:HomeRoute,
})

array.push({
	path:'/gallery',
	title:'Gallery',
	component:GalleryRoute,
})

///////////////////////////////////

export const routes = array;
