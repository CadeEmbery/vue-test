import _ from 'lodash'
import Vue from 'vue'
import App from './App.vue'
// import Fluro from './Fluro'
// import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import Axios from 'axios';



/////////////////////////////////////////////////////

export const session = new Vue({
	data() {
		return {
			user:null,
		}
	},
	methods:{
		setUser(userData) {
			console.log('SET USER SESSION', userData);
			this.user = userData;
		}
	}
});

/////////////////////////////////////////////////////

//Setup Axios for requests
Axios.defaults.baseURL = 'https://api.staging.fluro.io';
Axios.defaults.headers.common.Accept = 'application/json';

Vue.$http = Axios;
Object.defineProperty(Vue.prototype, '$http', {
	get() {
		return Axios;
	}
});


/////////////////////////////////////////////////////

Axios.interceptors.request.use((config) => {
	// console.log('REQUEST', config);

	var token = _.get(session, 'user.token');
	if(token) {
		config.headers['Authorization'] = 'Bearer ' + token;
		console.log('CONFIG', config);
	}
	

	return config;
}, (error) => {
	// console.log('REQUEST ERROR', error);

	return Promise.reject(error);
})

/////////////////////////////////////////////////////

Axios.interceptors.response.use((response) => {
	// console.log('RESPONSE', response);
	return response;
}, (error) => {
	// console.log('RESPONSE ERROR', error);

	return Promise.reject(error);
})

/////////////////////////////////////////////////////

import {
    routes
} from './routes/routes';

Vue.use(VueRouter);

const router = new VueRouter({
    routes: routes,
})

/////////////////////////////////////////////////////

//Set the API URL for Fluro
// Vue.http.options.root = 'https://api.staging.fluro.io/';

/////////////////////////////////////////////////////

// //Add an interceptor
// Vue.http.interceptors.push(function(request, next) {
// 	if(this.session && this.session.token) {
// 		Vue.http.options.credentials = false;
// 		console.log('INTERCEPT AND ADD HEADERS', this.session.token);
// 		request.headers.set('Authorization', 'Bearer ' + this.session.token);
// 	} else {
// 		Vue.http.options.credentials = true;
// 	}

// 	console.log('REQUEST', request);

// 	return next(function(response) {
// 		console.log('Response', response);
// 	});
// });





/////////////////////////////////////////////////////


new Vue({
    el: '#app',
    render: h => h(App),
    router: router,
})


